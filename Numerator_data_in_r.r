library(RODBC)

#this kicks off your kerberos session, just replace my info with yours below
system("kinit -kt /data/home/patricia.scully/patricia.scully.keytab patricia.scully@MILLER.LOCAL")

#this connects to the core db in Impala
database = 'core'
conn = odbcDriverConnect(paste0("DSN=impala;Database=",database), case="nochange")

#this queries the numerator table and imports data into an r dataframe
numerator_sample = sqlQuery(conn, paste("SELECT * FROM ", database,".numerator"),
                     stringsAsFactors = FALSE)